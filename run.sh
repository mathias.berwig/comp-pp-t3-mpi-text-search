echo +-------------------------------------+
echo +------- Iniciando mágica ------------+
echo +-------------------------------------+
echo +  para executar digite: bash run.sh  +
echo +-------------------------------------+

printf 'Sequencial' >> run.txt
for run in 1 2 3 4 5 6 7 8 9 10
do	
	echo -en "\n\e[1A\rRun: $run/10 Threads: 1/1  " 
	
	printf '\t' >> run.txt
	printf $(./2) >> run.txt
	printf '\n' >> run.txt
done
printf '\n' >> run.txt
printf '\n' >> run.txt

printf 'MPI' >> run.txt
for run in 1 2 3 4 5 6 7 8 9 10
do	
	for nt in 2 3 4 5 6 7 8 9 10 11
	do
    	echo -en "\n\e[1A\rRun: $run/10 Threads: $nt/11  " 

		printf '\t' >> run.txt
		printf $(mpirun -np $nt ./4) >> run.txt
	done
	printf '\n' >> run.txt
done
printf '\n' >> run.txt