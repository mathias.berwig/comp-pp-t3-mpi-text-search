# comp-pp-t3-mpi-text-search

## Resumo

Trabalho III - Busca de texto com MPI  
Data de entrega: 30/05/18  
Local de entrega: somente no portal  
Peso 15 pontos  
Individual

## Requisitos

- Encontrar paralelamente a quantidade de vezes que a sequência de caracteres está presente no texto do arquivo
- A busca deve considerar também a sequência invertida de caracteres
- execute em paralelo utilizando o compilador gcc com MPI 
- receba com argumento a qtde de processos que devem ser criados

## Entrega do trabalho:

Arquivo zipado no portal contendo:
- arquivo (MPI.c) 	 - implementação paralela com MPI
- arquivo (Makefile) - make
- arquivo tabela [Speed-up e Eficiência.ods](Speed-up e Eficiência.ods) - calculos de speed-up e eficiência com a média de 10 execuções e descrição do computador

## Avaliação do trabalho:

- 5 pontos - funcionalidade, lógica de paralelização / divisão do trabalho
- 10 pontos - Ganhos de tempo(speed-up) e eficiência obtida com a paralelização utilizando arquivo de 1 GB.  
	* top-down (início-fim do arquivo) - 5 pontos
	* botton-up (fim-início do arquivo) - 5 pontos

## Dica: 

Estudar e utilizar estas funções para realização o trabalho de:  
- Arquivos em C: `fopen(), fclose(), getc(), putc(), fread(), fwrite(), fseek()`  
- Comunicacao MPI: `MPI_Init(), MPI_Finalize(), MPI_Comm_size(), MPI_Comm_rank(), MPI_Send(), MPI_Recv()`